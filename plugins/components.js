import Vue from 'vue'

import Category1 from "../components/blocks/Category1";
import Category2 from "../components/blocks/Category2";
import Category3 from "../components/blocks/Category3";
import Category4 from "../components/blocks/Category4";
import Category5 from "../components/blocks/Category5";
import Contact1 from "../components/blocks/Contact1";
import Contact2 from "../components/blocks/Contact2";
import Contact3 from "../components/blocks/Contact3";
import Content1 from "../components/blocks/Content1";
import Content2 from "../components/blocks/Content2";
import Content3 from "../components/blocks/Content3";
import Content4 from "../components/blocks/Content4";
import Content5 from "../components/blocks/Content5";
import Content6 from "../components/blocks/Content6";
import Content7 from "../components/blocks/Content7";
import Content8 from "../components/blocks/Content8";
import Content9 from "../components/blocks/Content9";
import CTA1 from "../components/blocks/CTA1";
import CTA2 from "../components/blocks/CTA2";
import CTA3 from "../components/blocks/CTA3";
import CTA4 from "../components/blocks/CTA4";
import Feature1 from "../components/blocks/Feature1";
import Feature2 from "../components/blocks/Feature2";
import Feature3 from "../components/blocks/Feature3";
import Feature4 from "../components/blocks/Feature4";
import Feature5 from "../components/blocks/Feature5";
import Feature6 from "../components/blocks/Feature6";
import Feature7 from "../components/blocks/Feature7";
import Feature8 from "../components/blocks/Feature8";
import Feature9 from "../components/blocks/Feature9";
import Footer1 from "../components/blocks/Footer1";
import Footer2 from "../components/blocks/Footer2";
import Footer3 from "../components/blocks/Footer3";
import Footer4 from "../components/blocks/Footer4";
import Footer5 from "../components/blocks/Footer5";
import Gallery1 from "../components/blocks/Gallery1";
import Gallery2 from "../components/blocks/Gallery2";
import Gallery3 from "../components/blocks/Gallery3";
import Header1 from "../components/blocks/Header1";
import Header2 from "../components/blocks/Header2";
import Header3 from "../components/blocks/Header3";
import Header4 from "../components/blocks/Header4";
import Hero1 from "../components/blocks/Hero1";
import Hero2 from "../components/blocks/Hero2";
import Hero3 from "../components/blocks/Hero3";
import Hero4 from "../components/blocks/Hero4";
import Hero5 from "../components/blocks/Hero5";
import Hero6 from "../components/blocks/Hero6";
import Pricing1 from "../components/blocks/Pricing1";
import Pricing2 from "../components/blocks/Pricing2";
import Stats1 from "../components/blocks/Stats1";
import Stats2 from "../components/blocks/Stats2";
import Stats3 from "../components/blocks/Stats3";
import Step1 from "../components/blocks/Step1";
import Step2 from "../components/blocks/Step2";
import Step3 from "../components/blocks/Step3";
import Team1 from "../components/blocks/Team1";
import Team2 from "../components/blocks/Team2";
import Team3 from "../components/blocks/Team3";
import Testimonial1 from "../components/blocks/Testimonial1";
import Testimonial2 from "../components/blocks/Testimonial2";
import Testimonial3 from "../components/blocks/Testimonial3";
import ClassTime from '../components/blocks/ClassTime'
const components = {
  ClassTime,
  Category1,
  Category2,
  Category3,
  Category4,
  Category5,
  Contact1,
  Contact2,
  Contact3,
  Content1,
  Content2,
  Content3,
  Content4,
  Content5,
  Content6,
  Content7,
  Content8,
  Content9,
  CTA1,
  CTA2,
  CTA3,
  CTA4,
  Feature1,
  Feature2,
  Feature3,
  Feature4,
  Feature5,
  Feature6,
  Feature7,
  Feature8,
  Feature9,
  Footer1,
  Footer2,
  Footer3,
  Footer4,
  Footer5,
  Gallery1,
  Gallery2,
  Gallery3,
  Header1,
  Header2,
  Header3,
  Header4,
  Hero1,
  Hero2,
  Hero3,
  Hero4,
  Hero5,
  Hero6,
  Pricing1,
  Pricing2,
  Stats1,
  Stats2,
  Stats3,
  Step1,
  Step2,
  Step3,
  Team1,
  Team2,
  Team3,
  Testimonial1,
  Testimonial2,
  Testimonial3
};

Object.entries(components).forEach(([name, component]) => {
    Vue.component(name, component)
}) 