export const blocks = {
    custom: [
      {
        name: "ClassTime",
        img: "custom_class_time.svg",
        component: "ClassTime",
        type:'Custom',
      },
    ],
    header: [
      {
        name: "Header 1",
        img: "header_1.svg",
        component: "Header1",
        type:'Header',
        content: {
          logo: "",
          title: "Favored",
          links: [
            {
              label: "First Link",
              link: "#"
            },
            {
              label: "Second Link",
              link: "#"
            },
            {
              label: "Third Link",
              link: "#"
            },
            {
              label: "Fourth Link",
              link: "#"
            }
          ],
          button: {
            label: "Button",
            icon: "",
            link: "#"
          }
        }
      },
      {
        name: "Header 2",
        img: "header_2.svg",
        component: "Header2",
        type:'Header',
  
        content: {
          logo: "",
          title: "Favored",
          links: [
            {
              label: "First Link",
              link: "#"
            },
            {
              label: "Second Link",
              link: "#"
            },
            {
              label: "Third Link",
              link: "#"
            },
            {
              label: "Fourth Link",
              link: "#"
            }
          ],
          button: {
            label: "Button",
            icon: "",
            link: "#"
          }
        }
  
      },
      {
        name: "Header 3",
        img: "header_3.svg",
        component: "Header3",
        content: {
          logo: "",
          title: "Favored",
          links: [
            {
              label: "First Link",
              link: "#"
            },
            {
              label: "Second Link",
              link: "#"
            },
            {
              label: "Third Link",
              link: "#"
            },
            {
              label: "Fourth Link",
              link: "#"
            }
          ],
          button: {
            label: "Button",
            icon: "",
            link: "#"
          }
        }
      },
      {
        name: "Header 4",
        img: "header_4.svg",
        component: "Header4",
        content: {
          logo: "",
          title: "Favored",
          links: [
            {
              label: "First Link",
              link: "#"
            },
            {
              label: "Second Link",
              link: "#"
            },
            {
              label: "Third Link",
              link: "#"
            },
            {
              label: "Fourth Link",
              link: "#"
            }
          ],
          button: {
            label: "Button",
            icon: "",
            link: "#"
          }
        }
      }
    ],
    hero: [
      {
        name: "Hero 1",
        img: "hero_1.svg",
        component: "Hero1"
      },
      {
        name: "Hero 2",
        img: "hero_2.svg",
        component: "Hero2"
      },
      {
        name: "Hero 3",
        img: "hero_3.svg",
        component: "Hero3"
      },
      {
        name: "Hero 4",
        img: "hero_4.svg",
        component: "Hero4"
      },
      {
        name: "Hero 5",
        img: "hero_5.svg",
        component: "Hero5"
      },
      {
        name: "Hero 6",
        img: "hero_6.svg",
        component: "Hero6"
      }
    ],
    content: [
      {
        name: "Content 1",
        img: "content_1.svg",
        component: "Content1"
      },
      {
        name: "Content 2",
        img: "content_2.svg",
        component: "Content2"
      },
      {
        name: "Content 3",
        img: "content_3.svg",
        component: "Content3"
      },
      {
        name: "Content 4",
        img: "content_4.svg",
        component: "Content4"
      },
      {
        name: "Content 5",
        img: "content_5.svg",
        component: "Content5"
      },
      {
        name: "Content 6",
        img: "content_6.svg",
        component: "Content6"
      },
      {
        name: "Content 7",
        img: "content_7.svg",
        component: "Content7"
      },
      {
        name: "Content 8",
        img: "content_8.svg",
        component: "Content8"
      },
      {
        name: "Content 9",
        img: "content_9.svg",
        component: "Content9",
        type:'Content',
        content: {
          title: 'Space The Final Frontier',
          description: ' Street art subway tile salvia four dollar toast bitters selfies quinoa yuccie synth meditation iPhone intelligentsia prism tofu. Viral gochujang bitters dreamcatcher.',
          items: [
            {
              image: '',
              title: 'Shooting Stars',
              description: 'Blue bottle crucifix vinyl post-ironic four dollar toast vegan taxidermy. Gastropub indxgo juice poutine.'
            },
            {
              image: '',
              title: 'The Catalyzer',
              description: 'Blue bottle crucifix vinyl post-ironic four dollar toast vegan taxidermy. Gastropub indxgo juice poutine.'
            },
            {
              image: '',
              title: 'Neptune',
              description: 'Blue bottle crucifix vinyl post-ironic four dollar toast vegan taxidermy. Gastropub indxgo juice poutine.'
            }
          ]
        }
      }
    ],
    feature: [
      {
        name: "Feature 1",
        img: "feature_1.svg",
        component: "Feature1"
      },
      {
        name: "Feature 2",
        img: "feature_2.svg",
        component: "Feature2"
      },
      {
        name: "Feature 3",
        img: "feature_3.svg",
        component: "Feature3",
        
      },
      {
        name: "Feature 4",
        img: "feature_4.svg",
        component: "Feature4"
      },
      {
        name: "Feature 5",
        img: "feature_5.svg",
        component: "Feature5"
      },
      {
        name: "Feature 6",
        img: "feature_6.svg",
        component: "Feature6"
      },
      {
        name: "Feature 7",
        img: "feature_7.svg",
        component: "Feature7"
      },
      {
        name: "Feature 8",
        img: "feature_8.svg",
        component: "Feature8"
      },
      {
        
        name: "Feature 9",
      img: "feature_9.svg",
      component: "Feature9",
        type: 'Feature',
        content: {
          featuredImage: '',
          items: [
            {
              title: 'Shooting Stars',
              description: 'Blue bottle crucifix vinyl post-ironic four dollar toast vegan taxidermy. Gastropub indxgo juice poutine.'
            },
            {
              title: 'The Catalyzer',
              description: 'Blue bottle crucifix vinyl post-ironic four dollar toast vegan taxidermy. Gastropub indxgo juice poutine.'
            },
            {
              title: 'Neptune',
              description: 'Blue bottle crucifix vinyl post-ironic four dollar toast vegan taxidermy. Gastropub indxgo juice poutine.'
            }
          ]
          
        }
      }
    ],
    category: [
      {
        name: "Category 1",
        img: "cat_3_col1.svg",
        component: "Category1"
      },
      {
        name: "Category 2",
        img: "cat_3_col2.svg",
        component: "Category2"
      },
      {
        name: "Category 3",
        img: "cat_2_col.svg",
        component: "Category3"
      },
      {
        name: "Category 4",
        img: "cat_rows.svg",
        component: "Category4"
      },
      {
        name: "Category 5",
        img: "cat_3_col3.svg",
        component: "Category5"
      }
    ],
    cta: [
      {
        name: "CTA 1",
        img: "cta_1.svg",
        component: "CTA1"
      },
      {
        name: "CTA 2",
        img: "cta_2.svg",
        component: "CTA2"
      },
      {
        name: "CTA 3",
        img: "cta_3.svg",
        component: "CTA3"
      }
    ],
    step: [
      {
        name: "Step 1",
        img: "step_1.svg",
        component: "Step1"
      },
      {
        name: "Step 2",
        img: "step_2.svg",
        component: "Step2"
      },
      {
        name: "Step 3",
        img: "step_3.svg",
        component: "Step3"
      }
    ],
    team: [
      {
        name: "Team 1",
        img: "team_1.svg",
        component: "Team1"
      },
      {
        name: "Team 2",
        img: "team_2.svg",
        component: "Team2"
      },
      {
        name: "Team 3",
        img: "team_3.svg",
        component: "Team3"
      }
    ],
    testimonial: [
      {
        name: "Testimonial 1",
        img: "testimonial_1.svg",
        component: "Testimonial1"
      },
      {
        name: "Testimonial 2",
        img: "testimonial_2.svg",
        component: "Testimonial2"
      },
      {
        name: "Testimonial 3",
        img: "testimonial_3.svg",
        component: "Testimonial3"
      }
    ],
    pricing: [
      {
        name: "Pricing 1",
        img: "pricing_1.svg",
        component: "Pricing1"
      },
      {
        name: "Pricing 2",
        img: "pricing_2.svg",
        component: "Pricing2"
      }
    ],
    statistic: [
      {
        name: "Statistic 1",
        img: "stats_1.svg",
        component: "Statistic1"
      },
      {
        name: "Statistic 2",
        img: "stats_2.svg",
        component: "Statistic2"
      },
      {
        name: "Statistic 3",
        img: "stats_3.svg",
        component: "Statistic3"
      }
    ],
    footer: [
      {
        name: "Footer 1",
        img: "footer_1.svg",
        component: "Footer1"
      },
      {
        name: "Footer 2",
        img: "footer_2.svg",
        component: "Footer2"
      },
      {
        name: "Footer 3",
        img: "footer_3.svg",
        component: "Footer3"
      },
      {
        name: "Footer 4",
        img: "footer_4.svg",
        component: "Footer4"
      },
      {
        name: "Footer 5",
        img: "footer_5.svg",
        component: "Footer5"
      }
    ]
  };
  