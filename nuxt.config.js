export default {
  // Global page headers: https://go.nuxtjs.dev/config-head
  head: {
    title: 'Performer Suite Website',
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: '' },
      { name: 'format-detection', content: 'telephone=no' }
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' }
    ]
  },

  // Global CSS: https://go.nuxtjs.dev/config-css
  css: [
    'aos/dist/aos.css'
  ],

  // Plugins to run before rendering page: https://go.nuxtjs.dev/config-plugins
  plugins: [
    '~/plugins/components',
    {src: '~/plugins/aos.js', mode: 'client'}
  ],

  // Auto import components: https://go.nuxtjs.dev/config-components
  components: true,

  // Modules for dev and build (recommended): https://go.nuxtjs.dev/config-modules
  buildModules: [
    // https://go.nuxtjs.dev/tailwindcss
    '@nuxtjs/tailwindcss',
  ],

  // Modules: https://go.nuxtjs.dev/config-modules
  modules: [
    // https://go.nuxtjs.dev/axios
    '@nuxtjs/axios',
    // https://go.nuxtjs.dev/pwa
    '@nuxtjs/pwa',
    [
      "@nuxtjs/firebase",
      {
        config: {
          production: {
            apiKey: "AIzaSyBqXkVglti6NAJ9aP3AnGaqheMCBbUMvTw",
  authDomain: "performersuiteacademy.firebaseapp.com",
  databaseURL: "https://performersuiteacademy-default-rtdb.asia-southeast1.firebasedatabase.app",
  projectId: "performersuiteacademy",
  storageBucket: "performersuiteacademy.appspot.com",
  messagingSenderId: "80786670343",
  appId: "1:80786670343:web:84bb0827ad7be23d6b7ba2"
          },
          development: {
            apiKey: "AIzaSyBqXkVglti6NAJ9aP3AnGaqheMCBbUMvTw",
  authDomain: "performersuiteacademy.firebaseapp.com",
  databaseURL: "https://performersuiteacademy-default-rtdb.asia-southeast1.firebasedatabase.app",
  projectId: "performersuiteacademy",
  storageBucket: "performersuiteacademy.appspot.com",
  messagingSenderId: "80786670343",
  appId: "1:80786670343:web:84bb0827ad7be23d6b7ba2"
          }
        },
        services: {          
          database: true,
          firestore: true
        }
      }
    ]
  ],

  // Axios module configuration: https://go.nuxtjs.dev/config-axios
  axios: {},

  // PWA module configuration: https://go.nuxtjs.dev/pwa
  pwa: {
    manifest: {
      lang: 'en'
    }
  },

  // Build Configuration: https://go.nuxtjs.dev/config-build
  build: {
  }
}
