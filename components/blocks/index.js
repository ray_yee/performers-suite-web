import Category1 from "./Category1";
import Category2 from "./Category2";
import Category3 from "./Category3";
import Category4 from "./Category4";
import Category5 from "./Category5";
import Contact1 from "./Contact1";
import Contact2 from "./Contact2";
import Contact3 from "./Contact3";
import Content1 from "./Content1";
import Content2 from "./Content2";
import Content3 from "./Content3";
import Content4 from "./Content4";
import Content5 from "./Content5";
import Content6 from "./Content6";
import Content7 from "./Content7";
import Content8 from "./Content8";
import Content9 from "./Content9";
import CTA1 from "./CTA1";
import CTA2 from "./CTA2";
import CTA3 from "./CTA3";
import CTA4 from "./CTA4";
import Feature1 from "./Feature1";
import Feature2 from "./Feature2";
import Feature3 from "./Feature3";
import Feature4 from "./Feature4";
import Feature5 from "./Feature5";
import Feature6 from "./Feature6";
import Feature7 from "./Feature7";
import Feature8 from "./Feature8";
import Feature9 from "./Feature9";
import Footer1 from "./Footer1";
import Footer2 from "./Footer2";
import Footer3 from "./Footer3";
import Footer4 from "./Footer4";
import Footer5 from "./Footer5";
import Gallery1 from "./Gallery1";
import Gallery2 from "./Gallery2";
import Gallery3 from "./Gallery3";
import Header1 from "./Header1";
import Header2 from "./Header2";
import Header3 from "./Header3";
import Header4 from "./Header4";
import Hero1 from "./Hero1";
import Hero2 from "./Hero2";
import Hero3 from "./Hero3";
import Hero4 from "./Hero4";
import Hero5 from "./Hero5";
import Hero6 from "./Hero6";
import Pricing1 from "./Pricing1";
import Pricing2 from "./Pricing2";
import Stats1 from "./Stats1";
import Stats2 from "./Stats2";
import Stats3 from "./Stats3";
import Step1 from "./Step1";
import Step2 from "./Step2";
import Step3 from "./Step3";
import Team1 from "./Team1";
import Team2 from "./Team2";
import Team3 from "./Team3";
import Testimonial1 from "./Testimonial1";
import Testimonial2 from "./Testimonial2";
import Testimonial3 from "./Testimonial3";
import ClassTime from './ClassTime'
export default {
  ClassTime,
  Category1,
  Category2,
  Category3,
  Category4,
  Category5,
  Contact1,
  Contact2,
  Contact3,
  Content1,
  Content2,
  Content3,
  Content4,
  Content5,
  Content6,
  Content7,
  Content8,
  Content9,
  CTA1,
  CTA2,
  CTA3,
  CTA4,
  Feature1,
  Feature2,
  Feature3,
  Feature4,
  Feature5,
  Feature6,
  Feature7,
  Feature8,
  Feature9,
  Footer1,
  Footer2,
  Footer3,
  Footer4,
  Footer5,
  Gallery1,
  Gallery2,
  Gallery3,
  Header1,
  Header2,
  Header3,
  Header4,
  Hero1,
  Hero2,
  Hero3,
  Hero4,
  Hero5,
  Hero6,
  Pricing1,
  Pricing2,
  Stats1,
  Stats2,
  Stats3,
  Step1,
  Step2,
  Step3,
  Team1,
  Team2,
  Team3,
  Testimonial1,
  Testimonial2,
  Testimonial3
};
